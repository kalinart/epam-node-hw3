# UBER like service for freight trucks

in REST style, using MongoDB as database.

---

## Install

    $ git clone https://gitlab.com/kalinart/epam-node-hw3.git
    $ cd epam-node-hw3
    $ npm install

## Configure app

Create `.env` file in project folder, then edit it with your settings. You will need next constants:

- PORT=changeToYourPort
- DB_URL=changeToYourMongoDB_URL_connection
- TOKEN_SECRET=changeToYourSecret

## Running the project in dev mode

    $ npm run dev

## Running the project in production mode

    $ npm start
