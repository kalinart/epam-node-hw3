/* eslint-disable comma-dangle */
/* eslint-disable indent */
const Truck = require('../models/Truck');

// GET /api/trucks
const getTrucks = async (req, res) => {
  const {userId} = req.user;

  try {
    const trucks = await Truck.find({created_by: userId}, '-__v');
    res.json({trucks});
  } catch (error) {
    res.status(500).json({message: error.message});
  }
};

// POST /api/trucks
const createTruck = async (req, res) => {
  const {type} = req.body;
  const {userId} = req.user;

  if (!type) {
    return res.status(400).json({message: `Please specify 'type' parameter`});
  }

  try {
    const truck = new Truck({type, created_by: userId});
    await truck.save();

    res.json({message: 'Truck created successfully'});
  } catch (error) {
    res.status(400).json({message: error.message});
  }
};

// GET /api/trucks/:id
const getUserTruckById = async (req, res) => {
  const {id} = req.params;
  const {userId} = req.user;

  try {
    const truck = await Truck.findOne({_id: id, created_by: userId}, '-__v');
    if (!truck) {
      throw new Error('Truck not found');
    }
    res.json({truck});
  } catch (error) {
    res.status(400).json({message: error.message});
  }
};

// PUT /api/trucks/:id
const updateUserTruckById = async (req, res) => {
  const {type} = req.body;
  const {id} = req.params;
  const {userId} = req.user;

  try {
    const truck = await Truck.findOne({_id: id, created_by: userId});

    if (!truck) {
      throw new Error('Truck not found');
    }

    truck.type = type;
    await truck.save();

    res.json({message: 'Truck details changed successfully'});
  } catch (error) {
    res.status(400).json({message: error.message});
  }
};

// DELETE /api/trucks/:id
const deleteUserTruckById = async (req, res) => {
  const {id} = req.params;
  const {userId} = req.user;

  try {
    const truck = await Truck.findOneAndDelete({_id: id, created_by: userId});

    if (!truck) {
      return res.status(400).json({message: 'Truck not found'});
    }

    res.json({message: 'Truck deleted successfully'});
  } catch (error) {
    res.status(400).json({message: error.message});
  }
};

// POST /api/trucks/:id/assing
const assignUserTruckById = async (req, res) => {
  const {id} = req.params;
  const {userId} = req.user;

  try {
    // отасайнить
    const assingedTruck = await Truck.findOne({
      created_by: userId,
      assigned_to: userId,
    });

    if (assingedTruck) {
      assingedTruck.assigned_to = null;
      await assingedTruck.save();
    }

    // асайнить
    const truck = await Truck.findOne({_id: id, created_by: userId});

    if (!truck) {
      throw new Error('Truck not found');
    }

    truck.assigned_to = userId;
    await truck.save();

    res.json({message: 'Truck assigned successfully'});
  } catch (error) {
    res.status(400).json({message: error.message});
  }
};

module.exports = {
  createTruck,
  getTrucks,
  getUserTruckById,
  updateUserTruckById,
  deleteUserTruckById,
  assignUserTruckById,
};
