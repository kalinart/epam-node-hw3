/* eslint-disable comma-dangle */
/* eslint-disable indent */
const Load = require('../models/Load');
const Truck = require('../models/Truck');
const {getLoads} = require('../services/loadService');
const trucksTypes = require('../config/trucksTypes');
const loadState = require('../config/loadState');

// GET /api/loads
const getUserLoads = async (req, res) => {
  try {
    console.log(req.user);
    const loads = await getLoads(req.query, req.user);
    res.json({loads});
  } catch (error) {
    return res.status(400).json({message: error.message});
  }
};

// POST /api/loads
const createLoad = async (req, res) => {
  const {userId} = req.user;

  try {
    const truck = new Load({...req.body, created_by: userId});
    await truck.save();

    res.json({message: 'Load created successfully'});
  } catch (error) {
    res.status(400).json({message: error.message});
  }
};

// GET /api/loads/active
const getUserActiveLoad = async (req, res) => {
  const {userId} = req.user;

  try {
    const load = await Load.findOne({assigned_to: userId, status: 'ASSIGNED'});
    if (!load) {
      throw new Error('Load not found');
    }
    res.json({load});
  } catch (error) {
    res.status(400).json({message: error.message});
  }
};

// PATCH /api/loads/active/state
const iterateLoadState = async (req, res) => {
  const {userId} = req.user;

  try {
    const load = await Load.findOne({assigned_to: userId, status: 'ASSIGNED'});

    console.log(load);

    if (!load) {
      throw new Error('Load not found');
    }

    const {state} = load;

    const currentStateIndex = loadState.findIndex((st) => st === state);
    const nextState = loadState[currentStateIndex + 1];
    const lastState = loadState[loadState.length - 1];

    // 'Arrived to delivery'
    if (nextState === lastState) {
      load.status = 'SHIPPED';

      // Change Truck status
      const truck = await Truck.findOne({assigned_to: userId, status: 'OL'});

      if (!truck) {
        throw new Error('Truck not found');
      }

      truck.status = 'IS';
      await truck.save();
    }

    load.state = nextState;
    load.logs.push({message: `Load state changed to ${nextState}`});

    await load.save();

    res.json({message: `Load state changed to ${nextState}`});
  } catch (error) {
    res.status(400).json({message: error.message});
  }
};

// GET /api/loads/:id
const getUserLoadById = async (req, res) => {
  const {id} = req.params;

  try {
    const load = await Load.findById(id, '-__v');
    if (!load) {
      throw new Error('Load not found');
    }
    res.json({load});
  } catch (error) {
    res.status(400).json({message: error.message});
  }
};

// PUT /api/loads/:id
const updateUserLoadById = async (req, res) => {
  const {id} = req.params;
  const {userId} = req.user;

  try {
    const load = await Load.findOne({_id: id, created_by: userId});

    if (!load) {
      throw new Error('Load not found');
    }

    Object.assign(load, req.body);

    await load.save();

    res.json({message: 'Load details changed successfully'});
  } catch (error) {
    res.status(400).json({message: error.message});
  }
};

// DELETE /api/loads/:id
const deleteUserLoadById = async (req, res) => {
  const {id} = req.params;
  const {userId} = req.user;

  try {
    const load = await Load.findOneAndDelete({_id: id, created_by: userId});

    if (!load) {
      return res.status(400).json({message: 'Load not found'});
    }

    res.json({message: 'Load deleted successfully'});
  } catch (error) {
    res.status(400).json({message: error.message});
  }
};

// POST /api/loads/:id/post
const postUserLoadById = async (req, res) => {
  const {id} = req.params;
  const {userId} = req.user;

  // To post load should be:
  // - one truck with assign Driver in 'IS' status
  // - load should fit to this truck by Dimansion and Payload

  try {
    const load = await Load.findOne({
      created_by: userId,
      _id: id,
    });

    if (!load) {
      throw new Error('Load not found');
    }

    load.status = 'POSTED';

    // try find truck
    const trucks = await Truck.find({status: 'IS', assigned_to: {$ne: null}});
    const foundTruck = trucks.find((truck) => {
      const {type} = truck;
      const {payload, dimensions} = trucksTypes[type];
      return (
        payload >= load.payload &&
        dimensions.length >= load.dimensions.length &&
        dimensions.height >= load.dimensions.height &&
        dimensions.width >= load.dimensions.width
      );
    });

    // Truck not found
    if (!foundTruck) {
      load.status = 'NEW';
      load.logs.push({message: 'Truck not found'});
      await load.save();
      throw new Error('Truck not found');
    }

    // truck found
    foundTruck.status = 'OL';
    load.status = 'ASSIGNED';
    load.state = 'En route to Pick Up';
    load.assigned_to = foundTruck.assigned_to;
    load.logs.push({
      message: `Load assigned to driver with id ${foundTruck.assigned_to}`,
    });

    // Update truck and load
    await foundTruck.save();
    await load.save();

    res.json({message: 'Load posted successfully', driver_found: true});
  } catch (error) {
    res.status(400).json({message: error.message});
  }
};

// GET /api/loads/:id/shipping_info
const getUserLoadShippingDetailsById = async (req, res) => {
  const {id} = req.params;
  const {userId} = req.user;
  let loadDetails = {};

  try {
    const load = await Load.findOne({_id: id, created_by: userId}, '-__v');

    if (!load) {
      throw new Error('Load not found');
    }

    if (load.assigned_to) {
      const truck = await Truck.findOne(
        {assigned_to: load.assigned_to},
        '-__v'
      );
      if (truck) {
        loadDetails = {load, truck};
      }
    } else {
      loadDetails = {load};
    }

    res.json(loadDetails);
  } catch (error) {
    res.status(400).json({message: error.message});
  }
};

module.exports = {
  createLoad,
  getUserLoads,
  getUserLoadById,
  updateUserLoadById,
  deleteUserLoadById,
  postUserLoadById,
  getUserLoadShippingDetailsById,
  getUserActiveLoad,
  iterateLoadState,
};
