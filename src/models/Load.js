const mongoose = require('mongoose');

// const logSchema = new mongoose.Schema({
//   message: {
//     type: String,
//     required: true,
//   },
//   time: {
//     type: Date,
//     default: Date.now(),
//   },
// });

const loadSchema = new mongoose.Schema({
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  // assigned_to DriverID or TruckID? I think DriverID!
  assigned_to: {
    type: mongoose.Schema.Types.ObjectId,
    default: null,
  },
  status: {
    type: String,
    enum: {
      values: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'],
      message: `{VALUE} is not supported. Should be 
        "NEW", "POSTED", "ASSIGNED" or "SHIPPED"`,
    },
    trim: true,
    required: true,
    default: 'NEW',
  },
  state: {
    type: String,
    enum: {
      values: [
        'En route to Pick Up',
        'Arrived to Pick Up',
        'En route to delivery',
        'Arrived to delivery',
        null,
      ],
      message: `{VALUE} is not supported. 
      Should be "En route to Pick Up", "Arrived to Pick Up", 
      "En route to delivery" or "Arrived to delivery"`,
    },
    trim: true,
    default: null,
    // required: true,
  },
  name: {
    type: String,
    required: true,
  },
  payload: {
    type: Number,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  dimensions: {
    width: {
      type: Number,
      min: 0,
    },
    length: {
      type: Number,
      min: 0,
    },
    height: {
      type: Number,
      min: 0,
    },
  },
  // logs: [logSchema],
  logs: [
    {
      message: {
        type: String,
        required: true,
      },
      time: {
        type: Date,
        default: Date.now(),
      },
    },
  ],
  created_date: {
    type: Date,
    default: Date.now(),
  },
});

const Load = mongoose.model('Load', loadSchema);

module.exports = Load;
