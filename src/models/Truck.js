const mongoose = require('mongoose');

const truckSchema = new mongoose.Schema({
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  assigned_to: {
    type: mongoose.Schema.Types.ObjectId,
    default: null,
  },
  type: {
    type: String,
    enum: {
      values: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'],
      message: `{VALUE} is not supported. 
      Should be "SPRINTER", "SMALL STRAIGHT" or "LARGE STRAIGHT"`,
    },
    trim: true,
    required: true,
  },
  status: {
    type: String,
    enum: {
      values: ['IS', 'OL'],
      message: '{VALUE} is not supported. Should be "IS" or "OL"',
    },
    trim: true,
    required: true,
    default: 'IS',
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },
});

const Truck = mongoose.model('Truck', truckSchema);

module.exports = Truck;
