const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const regExpEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

// DeprecationWarning: Mongoose: `findOneAndUpdate()` and `findOneAndDelete()`
// without the `useFindAndModify` option set to false are deprecated.
// mongoose.set('useFindAndModify', false);

const userSchema = new mongoose.Schema({
  email: {
    type: String,
    required: [true, 'Email is required'],
    unique: true,
    trim: true,
    lowercase: true,
    match: [regExpEmail, 'Please fill a valid email address'],
  },
  password: {
    type: String,
    required: [true, 'Password is required'],
    minLength: [6, 'Minimum password length is 6 characters'],
    trim: true,
  },
  role: {
    type: String,
    enum: {
      values: ['SHIPPER', 'DRIVER'],
      message: '{VALUE} is not supported. Should be "SHIPPER" or "DRIVER"',
    },
    trim: true,
    required: [true, 'Role is required'],
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },
});

// hook after saving (doc, next)
// userSchema.post('save', function (doc, next) {
//   console.log(doc);
//   next();
// });

// eslint-disable-next-line space-before-function-paren
// const hashPassword = async function (next) {
//   const salt = await bcrypt.genSalt();
//   this.password = await bcrypt.hashSync(this.password, salt);
//   next();
// };

// hook before saving (next)
// hash password before saving and updeting
// eslint-disable-next-line space-before-function-paren
userSchema.pre('save', async function (next) {
  const salt = await bcrypt.genSalt();
  this.password = await bcrypt.hashSync(this.password, salt);
  next();
});
// userSchema.pre('updateOne', hashPassword);

const User = mongoose.model('User', userSchema);

module.exports = User;
