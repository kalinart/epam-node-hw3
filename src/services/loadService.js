/* eslint-disable operator-linebreak */
/* eslint-disable indent */
const Load = require('../models/Load');

const getLoads = async (query, user) => {
  let {status, offset, limit} = query;
  const {userId, role} = user;

  offset = parseInt(offset) || 0;
  limit = parseInt(limit) || 10;
  // status = status || '*';

  if (offset < 0) offset = 0;
  if (limit >= 50) limit = 50;
  if (limit < 0) limit = 10;

  const shipperQuery = status
    ? {created_by: userId, status}
    : {created_by: userId};

  const driverQuery = status
    ? {assigned_to: userId, status}
    : {assigned_to: userId};

  let loads = [];

  if (role === 'SHIPPER') {
    loads = await Load.find(shipperQuery, '-__v').skip(offset).limit(limit);
  }

  if (role === 'DRIVER') {
    loads = await Load.find(driverQuery, '-__v').skip(offset).limit(limit);
  }

  return loads;
};

module.exports = {
  getLoads,
};
