/* eslint-disable quote-props */
const truckParams = {
  SPRINTER: {
    payload: 1700,
    dimensions: {
      length: 300,
      height: 250,
      width: 170,
    },
  },
  'SMALL STRAIGHT': {
    payload: 2500,
    dimensions: {
      length: 500,
      height: 250,
      width: 170,
    },
  },
  'LARGE STRAIGHT': {
    payload: 4000,
    dimensions: {
      length: 700,
      height: 350,
      width: 200,
    },
  },
};

module.exports = truckParams;
