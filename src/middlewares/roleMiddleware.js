const checkDriver = (req, res, next) => {
  const {role} = req.user;

  if (role !== 'DRIVER') {
    return res.status(403).json({message: 'No access. You should be DRIVER'});
  }

  next();
};

const checkShipper = (req, res, next) => {
  const {role} = req.user;

  if (role !== 'SHIPPER') {
    return res.status(403).json({message: 'No access. You should be SHIPPER'});
  }

  next();
};

module.exports = {
  checkDriver,
  checkShipper,
};
