/* eslint-disable indent */
const {checkDriver} = require('../middlewares/roleMiddleware');
const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
const {
  getTrucks,
  createTruck,
  getUserTruckById,
  updateUserTruckById,
  deleteUserTruckById,
  assignUserTruckById,
} = require('../controllers/truckControllers');

// check driver role
router.use(checkDriver);

// /api/trucks
router.route('/').get(getTrucks).post(createTruck);

// /api/trucks/:id
router
  .route('/:id')
  .get(getUserTruckById)
  .put(updateUserTruckById)
  .delete(deleteUserTruckById);

// /api/trucks/:id/assign
router.route('/:id/assign').post(assignUserTruckById);

module.exports = router;
