/* eslint-disable indent */
const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
const {
  getProfileInfo,
  deleteProfile,
  changeProfilePassword,
} = require('../controllers/meControllers');

// /api/users/me
router.route('/').get(getProfileInfo).delete(deleteProfile);
// /api/users/me/password
router.route('/password').patch(changeProfilePassword);

module.exports = router;
