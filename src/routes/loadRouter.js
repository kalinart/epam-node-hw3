/* eslint-disable indent */
const {checkShipper, checkDriver} = require('../middlewares/roleMiddleware');
const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
const {
  getUserLoads,
  createLoad,
  getUserLoadById,
  updateUserLoadById,
  deleteUserLoadById,
  postUserLoadById,
  getUserLoadShippingDetailsById,
  getUserActiveLoad,
  iterateLoadState,
} = require('../controllers/loadControllers');

// check Shipper role
// router.use(checkShipper);

// /api/loads
router.route('/').get(getUserLoads).post(checkShipper, createLoad);

// /api/loads/active
router.route('/active').get(checkDriver, getUserActiveLoad);

// /api/loads/active/state
router.route('/active/state').patch(checkDriver, iterateLoadState);

// /api/loads/:id
router
  .route('/:id')
  .get(getUserLoadById)
  .put(checkShipper, updateUserLoadById)
  .delete(checkShipper, deleteUserLoadById);

// /api/loads/:id/post
router.route('/:id/post').post(checkShipper, postUserLoadById);

// /api/loads/:id/shipping_info
router
  .route('/:id/shipping_info')
  .get(checkShipper, getUserLoadShippingDetailsById);

module.exports = router;
