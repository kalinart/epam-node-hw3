require('dotenv').config();
const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const PORT = process.env.PORT || 8080;
const DB_URL =
  process.env.DB_URL ||
  'mongodb+srv://dbuser:MjV7prYJSVundF5j@cluster0.mgsob.mongodb.net/uberdb?retryWrites=true&w=majority';

const {authMiddleware} = require('./middlewares/authMiddleware');

const authRouter = require('./routes/authRouter');
const meRouter = require('./routes/meRouter');
const truckRouter = require('./routes/truckRouter');
const loadRouter = require('./routes/loadRouter');

const app = express();

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);

// app.use('/api/users/me', [authMiddleware], meRouter);

app.use(authMiddleware);
app.use('/api/users/me', meRouter);
app.use('/api/trucks', truckRouter);
app.use('/api/loads', loadRouter);

app.get('/', (req, res) => {
  res.send('work 222');
});

// eslint-disable-next-line space-before-function-paren
app.use(function (err, req, res, next) {
  console.error(err.stack);
  res.status(500).send('Something broke!');
});

const start = async () => {
  try {
    await mongoose.connect(DB_URL, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    });

    console.log('Connected to DB');

    app.listen(PORT, () => {
      console.log(`Server has been started on port ${PORT}`);
    });
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();
